#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:36361550:02c1e3e82438d99c00d4d3d82563c56e30fa2859; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:31798602:4f2b7800127fd1abd6c1fed2c0d63abb40513190 EMMC:/dev/block/bootdevice/by-name/recovery 02c1e3e82438d99c00d4d3d82563c56e30fa2859 36361550 4f2b7800127fd1abd6c1fed2c0d63abb40513190:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
